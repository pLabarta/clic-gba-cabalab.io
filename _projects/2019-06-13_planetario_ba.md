---
title: Planetario BA
date: 2019-06-13
image: /images/planetario/planetario2.jpg
---

Presentación del primer concierto de livecoding formato full dome en el marco
de la DemoDay de GridX. Tocaron prrr, munshkr e irisS, con visuales de flor
de fuego.
