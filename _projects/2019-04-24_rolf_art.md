---
title: Rolf Art
date: 2019-04-24
image: /images/rolf/rolf3.jpg
---

Intervención en el cierre de la muestra fotográfica "En tiempo real" de la
artista Vivian Galban. Performance by flor de fuego (de La Plata) e irisS,
tomando datos extraídos con Processing (sketch by Martín Silberkasten) de las
fotografías analógicas de Vivian digitalizadas, e ingresados en
SuperCollider.
